<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtistArtisttypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artist_artist_type', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('artist_id')->unsigned()->nullable();
            $table->integer('artist_type_id')->unsigned()->nullable();
            $table->timestamps();
        });
        Schema::table('artist_artist_type', function (Blueprint $table) {
            $table->foreign('artist_id')->references('id')->on('artists')->onDelete('set null');
            $table->foreign('artist_type_id')->references('id')->on('artist_types')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artist_artisttype');
    }
}
