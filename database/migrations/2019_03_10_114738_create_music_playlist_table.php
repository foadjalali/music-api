<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMusicPlaylistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('music_playlist', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('music_id')->unsigned()->nullable();
            $table->integer('playlist_id')->unsigned()->nullable();
            $table->timestamps();
        });
        Schema::table('music_playlist', function (Blueprint $table) {
            $table->foreign('music_id')->references('id')->on('musics')->onDelete('set null');
            $table->foreign('playlist_id')->references('id')->on('playlists')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('music_playlist');
    }
}
