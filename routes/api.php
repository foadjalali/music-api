<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->group(function(){
	
	Route::get('/user', function (Request $request) {
	    return $request->user();
	});

	Route::resource('playlists' , 'PlaylistController');
	//Route::resource('artists' , 'ArtistController');
	Route::post('artists' , 'ArtistController@store');
	Route::put('artists/{id}' , 'ArtistController@update');
	Route::delete('artists/{id}' , 'ArtistController@destroy');
	Route::post('albums' , 'AlbumController@store');
	Route::put('albums/{id}' , 'AlbumController@update');
	Route::delete('albums/{id}' , 'AlbumController@destroy');
	Route::post('musics' , 'MusicController@store');
	Route::put('musics/{id}' , 'MusicController@update');
	Route::delete('musics/{id}' , 'MusicController@destroy');
});
Route::get('artists' , 'ArtistController@index');
Route::get('artists/{id}' , 'ArtistController@show');
Route::post('register' , 'AuthController@register');
Route::post('login' , 'AuthController@login');
Route::get('albums' , 'AlbumController@index');
Route::get('albums/{id}' , 'AlbumController@show');
Route::get('musics' , 'MusicController@index');
Route::get('musics/{id}' , 'MusicController@show');
Route::get('albums/{id}/musics' , 'AlbumMusicController@index');
