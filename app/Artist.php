<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{
    public function musics()
    {
    	return $this->belongsToMany('App\Music');
    }
    public function artisttypes()
    {
    	return $this->belongsToMany('App\ArtistType' , 'artist_artist_type');
    }
}
