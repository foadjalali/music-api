<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Music extends Model
{
    public function artists()
    {
    	return $this->belongsToMany('App\Artist');
    }
    public function playlists()
    {
    	return $this->belongsToMany('App\Playlist');
    }
    public function genre()
    {
    	return $this->belongsTo('App\Genre');
    }
    public function album()
    {
    	return $this->belongsTo('App\Album');
    }
}
