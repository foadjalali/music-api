<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Playlist extends Model
{
    public function user()
    {
    	return $this->belongsTo('App\User');
    }
    public function musics()
    {
    	return $this->belongsToMany('App\Music');
    }
}
