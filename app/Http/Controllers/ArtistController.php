<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Artist;
use App\User;

class ArtistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $artists = Artist::all();
        return $artists;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required' ,
            'bio'=>'required',
        ]);
        $user = Auth::user();
        if ($user->email == "root@root.com")
        {
            $artists = new Artist;
            $artists->name = $request->name;
            $artists->bio = $request->bio;
            $artists->save();
            return response()->json(['message'=>'artist added successfully']);
            
        }
        else
        {
            return response()->json(['message'=>'you are can not deleted you do not permission'] , 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $artists = Artist::all();
        $artist = $artists->where('id' , $id);
        return $artist;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            'bio'=>'required',
        ]);
        $user = $request->user()
        $user = Auth::user();
        if ($user->email == "root@root.com")
        {
            $update->name = $request->name;
            $update->bio = $request->bio;
            $update->save();
            return response()->json(['message'=>'artist updated successfully']);
        }
        
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $artists = Artist::all();
        $dlt = $artists->where('id' , $id)->first();
        $user = Auth::user();
        if ($user->email == "root@root.com")
        {
            $dlt->delete();
            return response()->json(['message'=>'artist deleted successfully']);
        }
        else
        {
            return response()->json(['message'=>'you are can not deleted item']);
        }
    }
}
