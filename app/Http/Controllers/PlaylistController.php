<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Playlist;
use Auth;

class PlaylistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $playlists = $user->playlists;
        return $playlists;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            
            'name'=>'required',
            
        ]);
        $playlist = new Playlist;
        $playlist->user_id = $request->user()->id;
        $playlist->name = $request->name;
        
        $playlist->save();
        return response()->json(['message'=>'playlist added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $user = Auth::user();
        $playlist = $user->playlists()->where('id', $id)->first();

        return $playlist;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
        ]);
        $user = $request->user()
        $update = $user->playlists()->where('id' , $id)->first();
        // $update = Playlist::findOrFail($id);
        $update->name = $request->name;
        $update->save();

        return response()->json(['message'=>'playlist updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request , $id)
    {
        $user = $request->user()
        $dlt = $user->playlists()->where('id' , $id)->first();
        $dlt->delete();
        return response()->json(['message'=>'playlist deleted successfully']);
    }
}
