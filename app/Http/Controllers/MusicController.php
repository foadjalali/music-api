<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Music;
use Auth;
use App\Album;
class MusicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $musics = Music::all();
        return $musics;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required' ,
            'file_address'=>'required' ,
            'image_address'=>'required' ,
            'album_id'=>'',
        ]);
        $user = Auth::user();
        if ($user->email == "root@root.com")
        {
            $music = new Music;
            $music->name = $request->name;
            $music->file_address = $request->file_address;
            $music->image_address= $request->image_address;
            $music->album_id = $request->album_id;
            $music->save();
            return response()->json(['message'=>'music added successfully']);
        }
        else
        {
            return response()->json(['message'=>'you can not added'],403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $musics = Music::findOrFail($id);
        return $musics;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required' ,
            'file_address'=>'required' ,
            'image_address'=>'required' ,
            'album_id'=>'',
        ]);
        $user = Auth::user();
        if ($user->email == "root@root.com")
        {
            $update = Music::where('id' , $id)->first();
            $update->name = $request->name;
            $update->file_address = $request->file_address;
            $update->image_address= $request->image_address;
            $update->album_id = $request->album_id;
            $update->save();
            return response()->json(['message'=>'music updated successfully']);
        }
        else
        {
            return response()->json(['message'=>'you can not updated'],403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
