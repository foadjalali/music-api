<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Str;

class AuthController extends Controller
{
	// public function __construct()
	// {
	//     $this->middleware('auth');
	// }
    public function register(Request $request)
    {
    	// return $request;
    	$request->validate([
    		'email'=>'required | unique:users',
    		'name'=>'required',
    		'password'=>'required',
    	]);
    	$user=new User;
    	$user->email = $request->email;
    	$user->name = $request->name;
    	$user->password = $request->password;
    	$user->save();
    	return response()->json(['message'=>'user added successfully']);
    }
    public function login(Request $request)
    {
        $request->validate([
    		'email'=>'required',
    		
    		'password'=>'required',
    	]);
    	$user = User::where('email' , $request->email)->where('password' , $request->password)->first();
    	if($user)
    	{
    		$token = Str::random(60);
    		$user->api_token = $token;
    		$user->save();
    		return response()->json(['message'=>'you are login', 'token' => $token]);
    	}
    	else
    	{
    		return response()->json(['message'=>'user not exist']);
    	}
    }
}
