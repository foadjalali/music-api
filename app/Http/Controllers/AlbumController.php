<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Album;
use Auth;
class AlbumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $album = Album::all();
        return $album;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required' ,
            'desc'=>'required' ,
        ]);
        $user = Auth::user();
        if ($user->email == "root@root.com")
        {
            $album = new Album;
            $album->name = $request->name;
            $album->desc = $request->desc;
            $album->save();
            return response()->json(['message'=>'album added successfully']);
        }
        else
        {
            return response()->json(['message'=>'you can not added'],403);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $album = Album::findOrFail($id);
        return $album;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required' ,
            'desc'=>'required' ,
        ]);
        $user = Auth::user();
        if ($user->email == "root@root.com")
        {
            $albums = Album::all();
            $update = $albums->where('id' , $id)->first();
            $update->name = $request->name;
            $update->desc = $request->desc;
            $update->save();
            return response()->json(['message'=>'album updated successfully']);
        }
        else
        {
            return response()->json(['message'=>'you can not updated'],403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $user = Auth::user();
        if ($user->email == "root@root.com")
        {
            $albums = Album::all();
            $dlt = $albums->where('id' , $id)->first();
            $dlt->delete();
            
            return response()->json(['message'=>'album delelted successfully']);
        }
        else
        {
            return response()->json(['message'=>'you can not updated'],403);
        }  
        
    }
}
